import axios from 'axios';
import {key, proxy} from '../config';

export default class Search {
	constructor(query) {
		this.query = query;
	}

	async getResults(){
		const proxy = 'https://cors-anywhere.herokuapp.com/';
		const key = 'e8fb29189b5ee5750a86458a2ba0a58e';
		try {
			const res = await axios(`${proxy}https://food2fork.com/api/search?key=${key}&q=${this.query}`);
			this.result = res.data.recipes;
			//console.log(this.result);
		} catch(e) {
			alert(e);
		}
	}
}