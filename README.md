#Food recipe app

JS application created using modern Javascript technologies like NPM, Webpack and Babel.  

###DESCRIPTION:

- This application is using Food2Fork API (https://www.food2fork.com/about/api).
- User can search for recipes for making food like pizza, pasta ... 
- After he write key words in search bar, the application will search for all recipes that API provides and list all of them in left list column.
- After user select wanted food title app will show recipe.
- In recipe section user can increase or decrease servings and app will instantly calculate values of ingredients.
- In that section user can also add ingredients to shop list and add recipe to favorits or delete it from there.
- From shop list ingredients can be deleted or increased.
- App is using Local Storage so favorits will show after page is reloaded.